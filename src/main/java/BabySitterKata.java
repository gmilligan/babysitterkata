import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class BabySitterKata {

    //constants
    private final String[] familyAHours = {"11:00PM"};
    private final double[] familyAPrices = {15, 20};

    private final String[] familyBHours = {"10:00PM", "12:00AM"};
    private final double[] familyBPrices = {12, 8, 16};

    private final String[] familyCHours = {"9:00PM"};
    private final double[] familyCPrices = {21, 15};

    private final String hourTimeFormat = "h:mma";
    private final String earliestStartTime = "5:00PM";
    private final int maxHoursInShift = 11;

    //set properties
    private String familyLetter;
    private String startHourString;
    private String endHourString;

    public static void main(String[] args) {

    }

    private String sanitizeHourString(String unsanitizedHourString){

        unsanitizedHourString = unsanitizedHourString.replaceAll(" ", "");
        unsanitizedHourString = unsanitizedHourString.toUpperCase();

        return unsanitizedHourString;
    }

    private Calendar getCalendarHour(String hourString){

        DateFormat simpleDateFormat = new SimpleDateFormat(hourTimeFormat);

        Calendar calendar = Calendar.getInstance();

        try {
            Date date = simpleDateFormat.parse(hourString);

            calendar.setTime(date);

        } catch (ParseException e) {
            e.printStackTrace();
            calendar = null;
        }

        return calendar;
    }

    private String[] getOrderedArrayOfAllPossibleWorkHours(){

        String[] workHoursRangeArray = new String[maxHoursInShift + 1];

        SimpleDateFormat hourFormat = new SimpleDateFormat(hourTimeFormat);

        Calendar startHourCalendar = this.getCalendarHour(earliestStartTime);

        if(startHourCalendar!=null) {

            //loop through each hour of the shift, chronologically
            for (int h = 0; h < maxHoursInShift + 1; h++) {
                Date date = startHourCalendar.getTime();
                workHoursRangeArray[h] = hourFormat.format(date);
                startHourCalendar.add(Calendar.HOUR_OF_DAY, 1);
            }

        }

        return workHoursRangeArray;
    }

    private int[] getStartAndEndHoursIndexes() {

        String[] workHoursPossible = this.getOrderedArrayOfAllPossibleWorkHours();

        int indexOfStart = -1;
        int indexOfEnd = -1;

        for(int s = 0; s < workHoursPossible.length; s++){
            if(workHoursPossible[s] != null) {
                if (workHoursPossible[s].equals(startHourString)) {
                    indexOfStart = s;
                }
                if (workHoursPossible[s].equals(endHourString)) {
                    indexOfEnd = s;
                }
            }
        }

        int[] startEnd = new int[2];
        startEnd[0] = indexOfStart;
        startEnd[1] =indexOfEnd;

        return startEnd;
    }

    public void setFamilyLetter(String letter) {
        familyLetter = letter;
    }

    public String getFamilyLetter(){
        return familyLetter;
    }

    public void setStartHourString(String hourString) {
        startHourString = this.sanitizeHourString(hourString);
    }

    public String getStartHourString() {
        return startHourString;
    }

    public void setEndHourString(String hourString) {
        endHourString = this.sanitizeHourString(hourString);
    }

    public String getEndHourString() {
        return endHourString;
    }

    public double getHourFloat(String hourString) {

        double hourDouble = -1.0;

        Calendar calendar = this.getCalendarHour(hourString);

        if(calendar != null) {
            int hourInt = calendar.get(Calendar.HOUR_OF_DAY);
            int minuteInt = calendar.get(Calendar.MINUTE);

            hourDouble = (double)hourInt + (minuteInt / 60.0);
        }

        return hourDouble;
    }

    public boolean isFractionalHour(String hourString) {
        boolean isFractional = true;

        double hourDouble = this.getHourFloat(hourString);

        if((hourDouble == Math.floor(hourDouble)) && !Double.isInfinite(hourDouble)){
            isFractional = false;
        }

        return isFractional;
    }

    public boolean isBadReversedHourOrder() {
        boolean isBadOrder = true;

        int[] startEndHoursIndex = this.getStartAndEndHoursIndexes();
        if(startEndHoursIndex[0] <= startEndHoursIndex[1]){
            isBadOrder = false;
        }

        return isBadOrder;
    }

    public boolean isOutOfHoursRange() {
        boolean isOutOfRange = false;

        int[] startEndHoursIndex = this.getStartAndEndHoursIndexes();

        if(startEndHoursIndex[0] == -1 || startEndHoursIndex[1] == -1){
            isOutOfRange = true;
        }

        return isOutOfRange;
    }

    public double getCalculatedPay() {
        double calculatedPrice = 0;

        double[] prices = null;
        String[] hours = null;

        switch(familyLetter){
            case "A":  prices = familyAPrices; hours = familyAHours; break;
            case "B":  prices = familyBPrices; hours = familyBHours; break;
            case "C":  prices = familyCPrices; hours = familyCHours; break;
        }

        String[] possibleWorkHours = this.getOrderedArrayOfAllPossibleWorkHours();
        int[] startEndIndexes = this.getStartAndEndHoursIndexes();

        int currentIndex = 0;
        double currentPrice = prices[currentIndex];

        //loop through each hour that was worked
        for(int w = startEndIndexes[0]; w < startEndIndexes[1]; w++){

            String thisHour = possibleWorkHours[w];

            if(currentIndex + 1 <= hours.length) {
                //if on this hour, the payment changes
                if (hours[currentIndex].equals(thisHour)) {
                    currentIndex++;
                    //change to the new payment for this hour
                    currentPrice = prices[currentIndex];
                }
            }

            //assign a payment for this particular hour
            calculatedPrice += currentPrice;
        }

        return calculatedPrice;
    }


}
