import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class BabySitterKataTest {

    BabySitterKata babySitterKata;

    @Before
    public void setUp(){
        babySitterKata = new BabySitterKata();
    }

    @Test
    public void whenBabySitterKataSetsFamilyAItReturnsFamilyA(){
        String letter = "A";
        babySitterKata.setFamilyLetter(letter);
        assertEquals(letter, babySitterKata.getFamilyLetter());
    }

    @Test
    public void whenBabySitterKataSetsStartHourToFivePMItReturnsFivePM(){
        String hour = "5:00PM";
        babySitterKata.setStartHourString(hour);
        assertEquals(hour, babySitterKata.getStartHourString());
    }

    @Test
    public void whenBabySitterKataSetsEndHourToFourAMItReturnsFourAM(){
        String hour = "4:00AM";
        babySitterKata.setEndHourString(hour);
        assertEquals(hour, babySitterKata.getEndHourString());
    }

    @Test
    public void whenBabySitterKataSetsEndHourToFourAMWithSpaceItReturnsFourAMNoSpace(){
        String hour = "4:00 AM";
        babySitterKata.setEndHourString(hour);
        assertEquals("4:00AM", babySitterKata.getEndHourString());
    }

    @Test
    public void whenBabySitterKataSetsEndHourToFourAMLowercaseItReturnsFourAMUppercase(){
        String hour = "4:00am";
        babySitterKata.setEndHourString(hour);
        assertEquals("4:00AM", babySitterKata.getEndHourString());
    }

    @Test
    public void whenBabySitterKataGetsPassedFiveFortyFivePMStringItReturnsTheHourDoubleSeventeenPointSevenFive(){
        String hour = "5:45PM";
        assertEquals(17.75, babySitterKata.getHourFloat(hour), .001);
    }

    @Test
    public void whenBabySitterKataGetsPassedOnePMStringItReturnsTheHourDoubleThirteen(){
        String hour = "1:00PM";
        assertEquals(13.0, babySitterKata.getHourFloat(hour), .001);
    }

    @Test
    public void whenBabySitterKataGetsPassedFiveFortyFivePMStringItReturnsIsFractionalHourTrue(){
        String hour = "5:45PM";
        assertEquals(true, babySitterKata.isFractionalHour(hour));
    }

    @Test
    public void whenBabySitterKataGetsPassedTwoPMStringItReturnsIsFractionalHourFalse(){
        String hour = "2:00PM";
        assertEquals(false, babySitterKata.isFractionalHour(hour));
    }

    @Test
    public void whenBabySitterKataHasSetAStartHourThatOccursAfterEndHourItReturnsIsBadHourOrderTrue(){
        babySitterKata.setStartHourString("4:00AM");
        babySitterKata.setEndHourString("5:00PM");
        assertEquals(true, babySitterKata.isBadReversedHourOrder());
    }

    @Test
    public void whenBabySitterKataHasSetAStartHourThatOccursBeforeEndHourItReturnsIsBadHourOrderFalse(){
        babySitterKata.setStartHourString("5:00PM");
        babySitterKata.setEndHourString("4:00AM");
        assertEquals(false, babySitterKata.isBadReversedHourOrder());
    }

    @Test
    public void whenBabySitterKataHasSetFamilyAAndWorksFromTenPMToFourAMItReturnsOneHundredFifteen(){
        babySitterKata.setFamilyLetter("A");
        babySitterKata.setStartHourString("10:00PM");
        babySitterKata.setEndHourString("4:00AM");
        assertEquals(115.00, babySitterKata.getCalculatedPay(), .001);
    }

    @Test
    public void whenBabySitterKataHasSetFamilyBAndWorksFromSixPMToThreeAMItReturnsOneHundredTwelve(){
        babySitterKata.setFamilyLetter("B");
        babySitterKata.setStartHourString("6:00PM");
        babySitterKata.setEndHourString("3:00AM");
        assertEquals(112.0, babySitterKata.getCalculatedPay(), .001);
    }

    @Test
    public void whenBabySitterKataHasSetFamilyCAndWorksFromEightPMToTenPMItReturnsThirtySix(){
        babySitterKata.setFamilyLetter("C");
        babySitterKata.setStartHourString("8:00PM");
        babySitterKata.setEndHourString("10:00PM");
        assertEquals(36.0, babySitterKata.getCalculatedPay(), .001);
    }

    @Test
    public void whenBabySitterKataHasSetStartHourBeforeEarliestStartItReturnsIsOutOfHoursRangeTrue(){
        babySitterKata.setStartHourString("4:00PM");
        babySitterKata.setEndHourString("6:00PM");
        assertEquals(true, babySitterKata.isOutOfHoursRange());
    }

    @Test
    public void whenBabySitterKataHasSetStartHourAtEarliestStartItReturnsIsOutOfHoursRangeFalse(){
        babySitterKata.setStartHourString("5:00PM");
        babySitterKata.setEndHourString("6:00PM");
        assertEquals(false, babySitterKata.isOutOfHoursRange());
    }


}
