# Build Tests

### Mac:

At the project root

`./gradlew build`

### Windows (cmd):

At the project root

`gradlew build`


## View Tests

On either Mac or Windows, you should now be able to navigate to the .html file,
to view the tests completed for this Kata

`build/reports/tests/test/classes/BabySitterKataTest.html`